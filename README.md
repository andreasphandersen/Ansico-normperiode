# Ansico-normperiode

Formålet med Ansico Normperiode er at lave 
et program hvor yngre læger kan beregne en 
række vigtige nøgletal baseret på deres 
individuelle normperiode.

# Licens 

Ansico Normperiode er udgivet under en 
GPLv3 licens.

# Version

Version 0.1 blev frigivet den 29. juli 2020 
af Andreas Andersen, Ansico.
